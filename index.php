<!DOCTYPE html>
<?php 
require_once 'db_connect.php'; //connect to database
include_once 'functions.php'; // common functions
// Assign variables
$row_class="odd"; // set variable to allow rows to be styled in alternating fashion
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Batch Details</title>
        <link rel="stylesheet" type="text/css" href="theme.css">
    </head>
    <body>
        <?php
        
        if (!isset($_GET['page']))
        {
            $page='dashboard.php';
            $header='Batch Details Dashboard';
        }
        else 
        {
            $header = $_GET['header']; 
            $page = $_GET['page'];             
        }
       
        echo "<h2>".$header."</h2>"; 
        include 'menu.php'; // Menu links
        include $page;
        
        ?> 
    </body>
</html>
