<?php
//Functions
function get_batch_details($db, $process_path)
{
      $query = "SELECT COUNT(*) AS total_batches, SUM(num_orders) as total_orders, SUM(num_units) as total_units FROM batch_details WHERE process_path ='" . $process_path . "'";
      $totals = $db->query($query);
      $total = $totals->fetch();

      $f_total_batches = $total['total_batches'];
      $f_total_orders = $total['total_orders'];
      $f_total_units = $total['total_units'];

      
      return array ($f_total_batches, $f_total_orders, $f_total_units);
}
function calc_batch_averages($total_batches, $total_orders, $total_units)
{
      $f_avg_units_per_order = $total_units / $total_orders;
      $f_avg_units_per_batch = $total_units / $total_batches;
      $f_avg_orders_per_batch = $total_orders / $total_batches;
      
      return array ($f_avg_units_per_order, $f_avg_units_per_batch, $f_avg_orders_per_batch);
}
function get_rate($db, $process_path)
{
      $query = "SELECT rate FROM rates WHERE process_path = '" . $process_path . "'";
      $rates = $db->query($query);
      $result = $rates->fetch();

      return $result['rate'];
}

print "<!--******************************************************************************--> 
        <h3>Batch Totals</h3>
        <table class='Grid'>
            <tr>
            <th>Process Path</th>
            <th>Total Batches</th>
            <th>Total Orders</th>
            <th>Total Units</th>         
            </tr>";
// Build Batch Totals table
        $pp_list = get_process_path_list($db);
        
        foreach ($pp_list as $pp) :  // pull data and make calculations for each process path 
            $process_path = $pp['process_path'];
            
            list($total_batches, $total_orders, $total_units) = get_batch_details($db, $process_path);
         
            echo "<tr class=\"$row_class\">"; //style row
            echo "<td>".$process_path."</td>";
            echo "<td>".$total_batches."</td>";
            echo "<td>".$total_orders."</td>";
            echo "<td>".$total_units."</td>";
            echo "</tr>";
            //alternate style for even and odd row class value            
            $row_class = change_row_class($row_class);
            
         endforeach; 
print"         </table>
<!--******************************************************************************-->
        <h3>Average Batch Details</h3>        
        <table class='Grid'>
            <tr>
                <th>Process Path</th>
                <th>Avg Units/Order</th>
                <th>Mean Units/Batch</th>
                <th>Mean Orders/Batch</th>
            </tr>";
// Build Average Batch Details table
        $pp_list = get_process_path_list($db);
          foreach ($pp_list as $pp) : // pull data and make calculations for each process path 
              $process_path = $pp['process_path'];
          
          list($total_batches, $total_orders, $total_units) = get_batch_details($db, $process_path);
          list($avg_units_per_order, $avg_units_per_batch, $avg_orders_per_batch) = calc_batch_averages($total_batches, $total_orders, $total_units);
              
            echo "<tr class=\"$row_class\">"; //style row
            echo "<td>".$process_path."</td>";
            echo "<td>".number_format($avg_units_per_order, 2)."</td>";
            echo "<td>".number_format($avg_units_per_batch, 2)."</td>";
            echo "<td>".number_format($avg_orders_per_batch, 2)."</td>";
            echo "</tr>";

            //alternate even and odd row class value
            $row_class = change_row_class($row_class);
            
          endforeach;
print"       </table>
<!--***********************************************************************************-->
        <h3>Average Order Throughput at 100% </h3>   
        <table class='Grid'>
            <tr>
                <th>Process Path</th>
                <th>Required Rate (UPH)</th>
                <th>Rate (avg OPH)</th>
                <th>Rate (avg OPM)</th>
                <th>Rate (avg MPO)</th>
            </tr>";
// Build Average Order Throughput table
         $pp_list = get_process_path_list($db);
          foreach ($pp_list as $pp) : // pull data and make calculations for each process path 
              $process_path = $pp['process_path'];
          
            list($total_batches, $total_orders, $total_units) = get_batch_details($db, $process_path);
            list($avg_units_per_order, $avg_units_per_batch, $avg_orders_per_batch) = calc_batch_averages($total_batches, $total_orders, $total_units);         
            $rate = get_rate($db, $process_path);
            
            $avg_orders_per_hour = $rate / $avg_units_per_order;
            $avg_orders_per_minute = $avg_orders_per_hour / 60;
            $avg_minutes_per_order = 60 / $avg_orders_per_hour;    
              
            echo "<tr class=\"$row_class\">"; //style row 
            echo "<td>".$process_path."</td>";
                echo "<td>".$rate."</td>";
                echo "<td>".number_format($avg_orders_per_hour, 2)."</td>";
                echo "<td>".number_format($avg_orders_per_minute, 2)."</td>";
                echo "<td>".number_format($avg_minutes_per_order, 2)."</td>";  
            echo"</tr>";            
            //alternate even and odd row class value            
            $row_class = change_row_class($row_class);           
         endforeach;
echo"        </table>";

