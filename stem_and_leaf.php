<?php
        $process_path = $_POST['pp']; //GET process path value
        
        echo "<h3>Stem and Leaf Diagram for ".$process_path."</h3>";
        
        $result_set_count=$db->query("SELECT COUNT(*) AS num_batches FROM batch_details WHERE process_path ='".$process_path."'");
        $results=$result_set_count->fetch();
        
        $num_batches = $results['num_batches'];
//Sets values for finding the median batch value(s) later. If number of batches is even, you use the average of the middle 2 batches to calculate the median       
        if ($num_batches%2==0) { // even
            $median_1 = $num_batches / 2; // these 2 lines give the comparison values to use to get the median batch values (even, so need 2 batch values)
            $median_2 = ($num_batches / 2) + 1;

//If the number of batches is odd, the middle batch is the median value.           
        }
        else { // odd
            $median_1 = ($num_batches + 1) / 2; // these 2 lines give the comparison values to use to get the median batch values (odd, so need 1 batch value)
            $median_2 = 0;
        }        
        $countdown = $num_batches; // used to iterate loop
        
        $result_set = $db->query("SELECT num_units DIV 10 as stem, num_units % 10 as leaf FROM batch_details WHERE process_path = '".$process_path."' ORDER BY stem, leaf");

     $last_stem = ''; // used to trigger new table row
print"     
     <table class='Grid'>
         <tr>
             <th>Stem</th>
             <th>Leaf <span style='color:red;font-size:.6em'>(RED = Median Batch)</span></th>
         </tr>";
            
$first_row= '';

foreach ($result_set as $stem_and_leaf) {
    
    $stem = $stem_and_leaf['stem'];
    $leaf = $stem_and_leaf['leaf'];
   
    if ($stem == $last_stem) //stay on same table row
    {
        if ($countdown == $median_1 or $countdown == $median_2) { // add additional style to indicate median batch(es)
            $median_style="<span style='color:red;font-size:1.1em'>";
            $close_median_style="</span>";
        
        }
        else { // if not median batch
            $median_style="";
            $close_median_style="";
        }
        echo $median_style, $leaf, $close_median_style." ";
    }
    else { // new stem, so need to start new table row
        if ($countdown == $median_1 or $countdown == $median_2) { // add additional style to indicate median batch(es)
            $median_style="<span style='color:red;font-size:1.1em'>";
            $close_median_style="</span>"; 
        }
        else { // not median batch
            $median_style="";
            $close_median_style="";
        }
        echo $first_row."<tr class=\"$row_class\"><td>".$stem."</td><td>".$median_style, $leaf, $close_median_style." ";
        $first_row="</td></tr>";
        $last_stem = $stem;
        $row_class = change_row_class($row_class);
    }

    $countdown = $countdown - 1;
} // loop

echo"        </table>";