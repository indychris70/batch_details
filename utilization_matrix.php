<?php
        $process_path = $_POST['pp']; //GET process path value
        // get data and calculations needed to populate table
         $query = "SELECT COUNT(*) AS total_batches, SUM(num_orders) as total_orders, SUM(num_units) as total_units FROM batch_details WHERE process_path ='" . $process_path . "'";
              $totals = $db->query($query);
              $total = $totals->fetch();
              
              $total_batches = $total['total_batches'];
              $total_orders = $total['total_orders'];
              $total_units = $total['total_units'];
              $avg_units_per_order = $total_units / $total_orders;
              $avg_units_per_batch = $total_units / $total_batches;
              $avg_orders_per_batch = $total_orders / $total_batches;
              
              $query = "SELECT rate FROM rates WHERE process_path = '" . $process_path . "'";
              $rates = $db->query($query);
              $result = $rates->fetch();
              
              $rate = $result['rate'];
       
print"        <h3>Utilization Matrix for ".$process_path."</h3>      
        <table class='Grid'>
            <tr>                
                <th>Utilization</th>
                <th>Minutes<br/>Packing</th>
                <th>Rate Needed<br/>to Avg <?php echo $rate; ?> UPH</th>
                <th>Percentage<br/>Increase in UPH</th>
                <th>Orders per<br/>Hour</th>
                <th>Orders per<br/>Minute</th>
                <th>Minutes per<br/>Order</th>
            </tr>  ";          

        $count=60;
        $row_class='odd'; // track even/odd row          
        while ($count > 0)  { // create a row for each minute change in utilization from 60 to 1
            $minutes_packing=$count;
            //utilization calculations
              $utilization = ($minutes_packing / 60) ;
              $rate_needed = $rate / $utilization;
              $percentage_increase = (($rate_needed / $rate) - 1) * 100;
              $avg_orders_per_hour = $rate_needed / $avg_units_per_order;
              $avg_orders_per_minute = $avg_orders_per_hour / 60;
              $avg_minutes_per_order = 60 / $avg_orders_per_hour; 
            // populate row  
            echo "<tr class=\"$row_class\">"; //style row
            echo "<td>".number_format($utilization * 100, 2)."%</td>" ;
            echo "<td>".$minutes_packing."</td>";
            echo "<td>".number_format($rate_needed, 0)."</td>" ;
            echo "<td>".number_format($percentage_increase, 2)."%</td>";
            echo "<td>".number_format($avg_orders_per_hour, 2)."</td>" ;
            echo "<td>".number_format($avg_orders_per_minute, 2)."</td>" ;
            echo "<td>".number_format($avg_minutes_per_order, 2)."</td>" ;
            echo "</tr>";
            
            $count = $count - 1;
            // alternate styling for each row
            $row_class = change_row_class($row_class);          
        } // loop 
        echo "</table>";