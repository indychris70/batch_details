<?php 
function add_batch($db, $date, $process_path, $num_orders, $num_units, $counter, $packing_time)
{        
    $query = "INSERT INTO batch_details (date, process_path, num_orders, num_units, packing_time) VALUES ('" . $date . "', '" . $process_path . "', '" . $num_orders . "', '" . $num_units . "', '" . $packing_time . "')";
    $result = $db->query($query);
    if ($counter == 1) {
        $batch_plural = 'batch';
    }
    else {
        $batch_plural = 'batches'; //select the grammatically correct word for the confirmation message.
    }
    echo "<p>" . $counter . " " . $batch_plural . " added.</p>"; // Confirmation message
    echo "<p>Last batch was a " . $process_path . " with " . $num_orders . " orders and " . $num_units . " units.</p>";
    }
        
if (isset($_REQUEST['add_batch'])) // If a batch update has been POSTed, update batch details and counter.
    {
    $counter = $_POST['counter'];
    $counter++;
    $date = $_POST['date'];
    $process_path = $_POST['process_path'];
    $previous_pp = $_POST['process_path'];
    $num_orders = $_POST['num_orders'];
    $num_units = $_POST['num_units'];
    $packing_time = $_POST['packing_time'];
    
    add_batch($db, $date, $process_path, $num_orders, $num_units, $counter, $packing_time);
    }
else {
    $counter=0;
}
print"
        <form name='Add Batch' action='index.php?page=new_batch.php&header=Add Batches' method='POST'>
        <table class='Grid'>
            <tr>
                <th>Description</th>
                <th>Batch Data</th>
            </tr>
            <tr class='odd'>
                <td><label>Enter Total Number of Orders: </label></td>
                <td><input type='text' name='num_orders' value='' size='5' /></td>
            </tr>
            <tr class='even'>
                <td><label>Enter Total Number of Units: </label></td>
                <td><input type='text' name='num_units' value='' size='5' /></td>
            </tr>
            <tr class ='odd'>                
                <td><label>Date (format YYYY-MM-DD): </label></td>
                <td><input type='text' name='date' value='";

            if (isset($date)){echo $date;} //set value of date field to last date selected, if applicable

print           "' size='20' /></td>
            </tr>
            <tr class='even'>
                <td><label>Select Process Path: </label></td>
                <td><select name='process_path'>
                        <option> </option>";

                            $query="SELECT process_path FROM process_paths"; // Populate drop down for Process Path input
                            $pp=$db->query($query);
                                foreach ($pp as $process_path) : {
                                        $selected = '';
                                        if (isset($previous_pp) )// sets selected value for process path drop down to last selection.
                                        {
                                            if ($previous_pp == $process_path['process_path']){
                                                $selected = 'selected';
                                            }
                                        }
                                        else {
                                            $selected = '';
                                        }
                                        echo "<option value='" . $process_path['process_path'] . "' " . $selected . ">" . $process_path['process_path'] . "</option>";
                                    }
                                    endforeach;
                                    echo "</select><br />";
print"
                </td>
            </tr>
            <tr class='odd'>
                <td><label>Enter total packing time (minutes): </label></td>
                <td><input type='text' name='packing_time' value='' /></td>
            </tr>
            <tr class='footer'>
                <td></td>
                <td><input type='hidden' name='counter' value='".$counter."' />
                    <input type='submit' value='Add Batch' name='add_batch' />
                </td>
            </tr>
        </table>
        </form> ";