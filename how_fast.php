<?php
print"
        <table class='Grid'>
            <tr>
                <th>Description</th>
                <th>Enter Values</th>
            </tr>
            <tr class='odd'>
                <td><form name='how_fast' action='index.php?page=how_fast_results.php&header=How Fast? Results' method='POST'>
                    <label>Average Utilization: </label>
                </td>
                <td><input type='text' name='utilization' value='' size='5' />%</td>
            </tr>
            <tr class='even'>
                <td><label>PPR for Process path: </label></td>
                <td><input type='text' name='PPR' value='' size='5' />%</td>
            </tr>
            <tr class='odd'>
                <td><label>Select Process Path: </label></td>
                <td><select name='process_path'>
            <option> </option>";

            $query="SELECT process_path FROM process_paths"; // Populate drop down for Process Path input
            $pp=$db->query($query);            
            foreach ($pp as $process_path) : {                                              
                $selected = '';  
                if (isset($previous_pp) )// sets selected value for process path drop down to last selection.
                {
                    if ($previous_pp == $process_path['process_path']){
                        $selected = 'selected';
                    }
                }
                else {
                    $selected = '';
                }
                echo "<option value='" . $process_path['process_path'] . "' " . $selected . ">" . $process_path['process_path'] . "</option>";
            }
            endforeach;

print "      </select><br />
                </td>
            </tr>
            <tr class='footer'>
                <td></td>
                <td><input type='submit' value='Calculate' name='how_fast' /></form></td>
            </tr>              
        </table>   ";     
