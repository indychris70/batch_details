<?php 
//Functions
function delete_batch($db, $id)
{        
    $query = "DELETE FROM batch_details WHERE id = '$id'";
    $delete_count = $db->exec($query);
    echo "<table class='Alert'><th>" . $delete_count . " row(s) deleted.</th></table>"; // Confirmation message
}
    
function update_batch($db, $id, $date, $process_path, $num_orders, $num_units, $packing_time)
{
    $query="UPDATE batch_details SET date='".$date."', process_path = '".$process_path."', num_orders = '".$num_orders."', num_units = '".$num_units."', packing_time = '".$packing_time."' WHERE id = '".$id."'";
    $update_count = $db->exec($query);
    echo "<table class='Alert'><th>" . $update_count . " row(s) updated.</th></table>"; // Confirmation message
}
        
            if (isset($_REQUEST['delete_batch'])) // If a batch deletion has been POSTed, delete the batch.
    {
            $id = $_POST['id'];
            delete_batch($db, $id);
    }
            if (isset($_REQUEST['update_batch'])) // If a batch update has been POSTed, update the batch.
    {
            $id = $_POST['id'];
            $date = $_POST['date'];
            $process_path = $_POST['process_path'];
            $num_orders = $_POST['num_orders'];
            $num_units = $_POST['num_units'];
            $packing_time = $_POST['packing_time'];
            update_batch($db, $id, $date, $process_path, $num_orders, $num_units, $packing_time);
    }
        $query= "SELECT * FROM batch_details ORDER BY id DESC" ;
        $batch_list=$db->query($query);//pull in data from batch_details     
        //create table headers
        echo "<table class='Grid' style='width: 80%'><tr>";
        echo "<th style ='width: 50px'>Delete<br/>Batch</th>";
        echo "<th style ='width: 50px'>ID</th>";
        echo "<th style ='width: 50px'>Date</th>";
        echo "<th style ='width: 50px'>Process<br/>Path</th>";
        echo "<th style ='width: 50px'>Number of Orders</th>";
        echo "<th style ='width: 50px'>Number of Units</th>";
        echo "<th style ='width: 50px'>Packing<br/>Time</th>";
        echo "<th style ='width: 50px'>Update<br>Batch</th>";
        echo "</tr> \n";      
        $row_class='odd'; // track even/odd rows       
        // Loop through all rows returned by $query, creating a row in the table for each
        foreach ($batch_list as $batch) :           
        //assign variables for each field in row
        $id=$batch['id'];
        $date=$batch['date'];
        $process_path=$batch['process_path'];
        $num_orders=$batch['num_orders'];
        $num_units=$batch['num_units'];
        $packing_time=$batch['packing_time'];       
            //populate fields in row
            echo "<tr class=\"$row_class\">"; //style row
            echo "<td><form name='Delete Batch' action='index.php?page=all_batches.php&header=Update/Delete Batches' method='POST'><input type='hidden' name='id' value='".$id."' /><input type='submit' value='Delete Batch' name='delete_batch' /></form>";
            echo "<td><form name='Update Batch' action='index.php?page=all_batches.php&header=Update/Delete Batches' method='POST'><input type='hidden' name='id' value='".$id."' /><label>".$id."</label></td>";
            echo "<td><input type='text' name='date' value='".$date."' size='10' /></td>";
            echo "<td><select name='process_path'><option> </option>";
                 $query="SELECT process_path FROM process_paths"; // Populate drop down for Process Path input
                    $pp=$db->query($query);
                    foreach ($pp as $pp_name) : {
                        $selected = '';
                        if ($pp_name['process_path'] == $process_path) // sets selected value for process path drop down to last selection.
                            {
                             $selected = 'selected';
                            }
                            else {
                                $selected = '';
                            }
                        echo "<option value='" . $pp_name['process_path'] . "' " . $selected . ">" . $pp_name['process_path'] . "</option>";
                    }
                    endforeach;
            echo "</select><br /></td>";
            echo "<td><input type='text' name='num_orders' value='".$num_orders."' size='5' /></td>";
            echo "<td><input type='text' name='num_units' value='".$num_units."' size='5' /></td>";
            echo "<td><input type='text' name='packing_time' value='".$packing_time."' size='5' /></td>";
            echo "<td><input type='submit' value='Update Batch' name='update_batch' /></form>";
            echo "</tr> \n";           
            //alternate even and odd row class value            
            $row_class = change_row_class($row_class);          
        endforeach;
        echo "</table>";             
            