<?php
        $intervals=$_POST['intervals'];
        $process_path=$_POST['pp'];
              
        echo "<h3>Frequency Table for ".$process_path."</h3>";
        
        $count=1; //variable to iterate loop
        
        $result_set=$db->query("SELECT COUNT(*) AS num_batches, MIN(num_units) AS min, MAX(num_units) AS max FROM batch_details WHERE process_path ='".$process_path."'");
        $results=$result_set->fetch();
        
        $num_batches = $results['num_batches'];
        
        $min = $results['min'];
        $max = $results['max'];
        
        $step = (($max - $min) / $intervals)+(($max-$min)/$intervals/$intervals); //$step is the range between intervals. max-min/intervals was not capturing the full range, so I added the additional fraction (max-min/intervals/intervals)
        $midpoint=$min;//set the initial midpoint, which will be adjusted upward by $step each iteration of loop
        $low_range_value=$midpoint - ($step / 2); //calculates the low value of the $step range
        $high_range_value=$midpoint + ($step / 2);//"              high value "        
        
        $total_frequency = 0; //to track totals for summary row
        $total_relative_frequency = 0;// ""   
print"
        <table class='Grid'>
            <tr>
                <th>Interval</th>
                <th>Midpoint</th>
                <th>Frequency</th>
                <th>Relative Frequency</th>
            </tr>";
            
        while ($count <= $intervals) {
            $result_set=$db->query("SELECT COUNT(*) AS frequency FROM batch_details WHERE process_path ='".$process_path."' AND num_units >='".$low_range_value."' AND num_units<'".$high_range_value."'");
            $result=$result_set->fetch();  // pulls count of rows in range set by high and low values
            
            $frequency = $result['frequency']; 
            $relative_frequency = ($frequency / $num_batches);
            
            echo "<tr class = \"$row_class\">";
            echo "<td>".number_format($low_range_value,2)."-".number_format($high_range_value,2)."</td>";
            echo "<td>".number_format($midpoint,2)."</td>";
            echo "<td>".$frequency."</td>";
            echo "<td>".number_format($relative_frequency,3)."</td>";
            echo "</tr>";
            
            $row_class=  change_row_class($row_class);  //alternate row styling
            
            $midpoint=$midpoint+$step; //next 3 calculations set the range for the next iteration of the loop
            $low_range_value = $midpoint - ($step / 2);
            $high_range_value=$midpoint + ($step / 2);
            
            $total_frequency = $total_frequency + $frequency;// next 2 lines update totals for  summary row
            $total_relative_frequency = $total_relative_frequency + $relative_frequency;
            
            $count = $count+1;    
        }       // loop
print"
            <tr class='footer'>
                <td><form name='Stem' action='index.php?page=stem_and_leaf.php&header=Stem and Leaf Diagram' method='POST'><input type='hidden' name='pp' value='".$process_path."' /><input type='submit' value='View Stem and Leaf Diagram' name='stem' /></form></td>
                <td>Totals</td>
                <td>".$total_frequency."</td>
                <td>".number_format($total_relative_frequency,3)."</td>
            </tr>
        </table>";

