<?php
        echo "<table class='Grid'>";
        echo "<tr>";
        echo "  <th>Process Path</th>";
        echo "  <th>Frequency Table</th>";
        echo "  <th>Stem and Leaf Diagram</th>";
        echo "  <th>How Fast Are My Packers?</th>";
        echo "  <th>Utilization Matrix</th>";            
        echo "</tr>";

       $pp_list = get_process_path_list($db);      
       foreach ($pp_list as $pp) :
           $process_path = $pp['process_path'];       
            echo "<tr class=\"$row_class\">";    
            echo "<td>".$process_path."</td>";
            echo "<td><form name=\"Frequency\" action=\"index.php?page=frequency.php&header=Frequency Table\" method=\"POST\">";
            echo "<label>Interval: </label><input type=\"text\" name=\"intervals\" value=\"10\" size=\"3\" />";
            echo "<input type=\"hidden\" name=\"pp\" value=".$process_path." />";
            echo "<input type=\"submit\" value=\"Go\" name=\"Frequency\" /></form></td>";
            echo "<td><form name='Stem' action='index.php?page=stem_and_leaf.php&header=Stem and Leaf Diagram' method='POST'><input type='hidden' name='pp' value='".$process_path."' /><input type='submit' value='View Stem and Leaf Diagram' name='stem' /></form></td>";
            echo "<td><form name='how_fast' action='index.php?page=how_fast.php&header=How fast are my packers <em>really</em> packing?' method='POST'><input type='submit' value='Check Speed' name='how_fast' /></form>";
            
            echo "<td><form name='utilization' action='index.php?page=utilization_matrix.php&header=Utilization Matrix' method='POST'><input type='hidden' name='pp' value='".$process_path."' /><input type='submit' value='View Utilization Matrix' name='utilization' /></form>";
            echo "</tr>";            
            $row_class = change_row_class($row_class);
       endforeach;