<?php          

$dsn = 'mysql:host=localhost;dbname=batch_stats';
$username = 'root';
$password = '';

try {
    $db = new PDO($dsn, $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $ex) {
    $error_message = $ex->getMessage();
    echo "<p>An error occured: $error_message</p>";
}
?>



